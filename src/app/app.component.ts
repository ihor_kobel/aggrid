import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { AllModules } from '@ag-grid-enterprise/all-modules';
import { ImageFormatterComponent } from './image-formatter/image-formatter.component';
import { StatusToolPanel } from './toolbar/status-tool-panel.component';
import { CustomHeader } from './header/custom-header.component';
import { MenuModule } from '@ag-grid-enterprise/menu';
import { ClipboardModule } from '@ag-grid-enterprise/clipboard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnInit {
    private gridApi;
    private youtubeData: any = [];
    private rowData1: any = [];
    public rowData: any = [];
    public modules: any[] = [AllModules, MenuModule, ClipboardModule];
    private icons;
    public sideBar;
    private checkboxSelect = true;
    public frameworkComponents;

    constructor(
        private appService: AppService
    ){
        this.frameworkComponents = { StatusToolPanel: StatusToolPanel , agColumnHeader: CustomHeader };
    }


    gridOptions = {
        sideBar: {
            toolPanels: [
                {
                    id: 'customStats',
                    labelDefault: 'ToolBar',
                    labelKey: 'customStats',
                    component: 'StatusToolPanel',
                    toolPanel: 'StatusToolPanel',
                }
            ],
            defaultToolPanel: 'StatusToolPanel',
          },
        rowHeight : 90,
        enableRangeSelection: true,
        getContextMenuItems: this.getContextMenuItems,
        onSelectionChanged: (event) => this.onSelectionChanged(),
    };
    columnDefs = [
        {headerComponentParams: {visible: this.checkboxSelect, enableCheck:true },
        field: 'thumbnails', cellRendererFramework: ImageFormatterComponent, resizable: true,
        checkboxSelection: this.checkboxSelect },
        {headerName: 'Published on', field: 'publishedAt', resizable: true },
        {headerName: 'Video Title', field: 'title', cellRenderer: (params) => params.value, resizable: true },
        {headerName: 'Description', field: 'description', resizable: true }
    ];

    mapData(el){
        return {
            thumbnails: el.snippet.thumbnails.default.url,
            publishedAt: el.snippet.publishedAt.substr(0, 10),
            title: '<a href="https://www.youtube.com/watch?v=' + el.id.videoId + '">'
             + el.snippet.title + '</a>',
            description: el.snippet.description
         }
    }

    ngOnInit() {
        this.appService.toggleSelection.subscribe(res => {
            if (res === 'yes') {
                this.checkboxSelect = !(this.checkboxSelect);
                this.gridApi.setColumnDefs([]);
                const columnDefs1 = [
                    {headerComponentParams: {visible: this.checkboxSelect, enableCheck:true },
                    field: 'thumbnails', cellRendererFramework: ImageFormatterComponent, resizable: true,
                    checkboxSelection: this.checkboxSelect },
                    {headerName: 'Published on', field: 'publishedAt', resizable: true },
                    {headerName: 'Video Title', field: 'title', cellRenderer: (params) => params.value, resizable: true },
                    {headerName: 'Description', field: 'description', resizable: true }
                ];
                this.gridApi.setColumnDefs(columnDefs1);
            }
          });

        this.appService.getDataResponse()
        .subscribe(resp => {
            this.youtubeData = { ... resp.body.items};
            this.rowData1 = Object.keys(this.youtubeData).map(i => this.youtubeData[i]);
            this.rowData = this.rowData1.map(this.mapData);
        });
    }

    onGridReady(params: any){
       this.gridApi = params.api;
    }

    onSelectionChanged(){
        this.appService.updatedSelection('yes');
    }

    getContextMenuItems(params) {
        let result =[];
        if (params && params.column) {
            if (params.column.colDef.headerName === 'Video Title') {
                let buffer = params.value;
                buffer = buffer.substr(buffer.indexOf('"') + 1);
                const url = buffer.substr(0, buffer.indexOf('"'));
                result = [
                    'copy',
                    'copyWithHeaders',
                    'paste',
                    {
                    name: 'Open in new tab',
                    action: ()  =>{
                        const win = window.open(url, '_blank');
                        win.focus();
                    }
                    }
                    ];
            }
        }
        return result;
    }
}
