import { Component, ViewChild, ViewContainerRef, OnDestroy } from '@angular/core';
import { AppService } from '../app.service';
import { IToolPanel, IToolPanelParams } from '@ag-grid-community/all-modules';
import { Subject } from 'rxjs/';

@Component({
  selector: 'custom-stats',
  templateUrl : './status-tool-panel.component.html',
  styleUrls: ['./status-tool-panel.component.scss'],
})
export class StatusToolPanel implements IToolPanel, OnDestroy {
  private params: IToolPanelParams;
  constructor(
    private appService: AppService
  ){}
  rowNumber: number;
  selectedRowNumber = 0;
  ngUnsubscrbe = new Subject<boolean>();

  agInit(params: IToolPanelParams): void {
    this.appService.updateSelection.subscribe(res => {
      if (res === 'yes') {
        this.onSelect();
      }
    });
    this.params = params;

    this.params.api.addEventListener(
      'modelUpdated',
      this.updateTotals.bind(this)
    );
  }

  refresh(){
    return true;
  }

  onToggleSelection(){
    this.params.api.deselectAll();
    this.appService.toggledSelection('yes');
  }

  onSelect(){
    this.selectedRowNumber = this.params.api.getSelectedRows().length;
  }

  updateTotals(): void {
    this.rowNumber = this.params.api.getModel().getRowCount();
  }

  ngOnDestroy() {
    if (this.ngUnsubscrbe) {
      this.ngUnsubscrbe.next(true);
      this.ngUnsubscrbe.complete();
      this.ngUnsubscrbe.unsubscribe();
    }
  }
}
