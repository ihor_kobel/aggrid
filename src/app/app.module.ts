import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
// import { AgGridModule  } from 'ag-grid-angular';
import { AgGridModule } from '@ag-grid-community/angular';
import { AppService } from './app.service';
import { ImageFormatterComponent } from './image-formatter/image-formatter.component';
import { StatusToolPanel } from './toolbar/status-tool-panel.component';
import { CustomHeader } from './header/custom-header.component';

@NgModule({
  declarations: [
    AppComponent,
    ImageFormatterComponent,
    StatusToolPanel,
    CustomHeader
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AgGridModule.withComponents([StatusToolPanel, ImageFormatterComponent, CustomHeader])
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
