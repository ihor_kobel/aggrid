import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, BehaviorSubject} from 'rxjs';
import { Subject } from 'rxjs';


@Injectable()
export class AppService {
  dataUrl = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyCA1HZ3OA3ZnsHrvyBkI7B08Fd_svBbmIk&maxResults=50&type=video&part=snippet&q=john';
  updateSelection = new BehaviorSubject<string>(null);
  toggleSelection = new BehaviorSubject<string>(null);  

  constructor(private http: HttpClient) { }

  getDataResponse(): Observable<HttpResponse<any>> {
    return this.http.get<any>(
      this.dataUrl, { observe: 'response' , responseType: 'json'});
  }

  updatedSelection(value: string) {
    this.updateSelection.next(value);
  }

  toggledSelection(value: string) {
    this.toggleSelection.next(value);
  }
}
