import { Component, ViewChild, ElementRef } from '@angular/core';


@Component({
  selector: 'app-loading-overlay',
  templateUrl: './custom-header.component.html',  
  styles: [],
})
export class CustomHeader {
  public params: any;
  public item =  false;

  @ViewChild('menuButton', { read: ElementRef }) public menuButton;

  agInit(params): void {
    this.params = params;

    params.column.addEventListener(
      'sortChanged',
    );
  }

  change(item, $event){
      if (this.item){
        this.params.api.deselectAll();
    } else {
        this.params.api.selectAll();
      }
      this.item = !(this.item);
  }
}
