import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { ModuleRegistry, AllModules } from '@ag-grid-enterprise/all-modules';

if (environment.production) {
  enableProdMode();
}

ModuleRegistry.registerModules(AllModules);
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
